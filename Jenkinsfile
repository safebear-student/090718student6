pipeline {
    agent any

    parameters {
        //test to run
        //API
        string(name:'apiTests', defaultValue:'ApiTestsIT', description:'API tests' )
        //Cucumber
        string(name:'cuke', defaultValue:'RunCukesIT', description:"Cucumber tests" )

        //general
        string(name:'context', defaultValue:'safebear', description:'context of the application')
        string(name:'domain', defaultValue:'http://52.12.7.56', description:'application base url' )

        //test environment
        string(name:'test_hostname', defaultValue:'52.12.7.56', description: 'hostname of test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')
    }

    options {
        buildDiscarder (logRotator(numToKeepStr:'3', artifactNumToKeepStr:'3'))
    }

    triggers { pollSCM('* * * * *') //Poll every 5 mins
    }

    stages {
        stage('Build with unit testing') {
            steps {
                sh 'mvn clean package'}
            post {
                success {
                    //only if successful
                    echo 'Now Archiving..'

                    archiveArtifacts artifacts: '**/target/*.war'
                }
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static analysis'){
            steps{
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success{
                    checkstyle canComputeNew: false, defaultEncoding:'', healthy:'', pattern:'', unHealthy:''
                }
            }
        }
        stage('Deployment to test'){
            steps{
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
        }
        stage('integration tests'){
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }

        }

    }
}
